#!/bin/bash

find {configuration,secrets}/$1 -type f \
  | grep --perl-regexp --invert-match "\.sample$|\.gen$|/.gitignore$" \
  | while read filename; do
      consul kv put "${filename}" "@${filename}"
    done
