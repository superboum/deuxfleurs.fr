job "directory2" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "ldap" {
    count = 1
    task "server" {
      driver = "docker"
      config {
        image = "lxpz/bottin_amd64:1"
        readonly_rootfs = true
        port_map {
          ldap_port = 1389
        }
        volumes = [
          "secrets/config.json:/config.json"
        ]
      }

      resources {
        memory = 100
        network {
          port "ldap_port" {
            static = "389"
          }
        }
      }

      template {
        data = "{{ key \"configuration/bottin/config.json\" }}"
        destination = "secrets/config.json"
      }

      service {
        tags = ["bottin"]
        port = "ldap_port"
        address_mode = "host"
        name = "bottin2"
        check {
          type = "tcp"
          port = "ldap_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

