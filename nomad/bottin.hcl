job "directory" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "ldap" {
    count = 2
    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_bottin:v10"
        readonly_rootfs = true
        port_map {
          ldap_port = 1389
        }
      }

      env {
        TRIGGER = 2
        BOTTIN_PORT = 1389
        BOTTIN_SUFFIX = "dc=deuxfleurs,dc=fr"
        BOTTIN_CONSUL = "consul.service.2.cluster.deuxfleurs.fr"
      }

      resources {
        memory = 100
        network {
          port "ldap_port" {
            static = "389"
          }
        }
      }

      service {
        tags = ["bottin"]
        port = "ldap_port"
        address_mode = "host"
        name = "bottin"
        check {
          type = "tcp"
          port = "ldap_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

