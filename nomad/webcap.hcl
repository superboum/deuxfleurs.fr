job "webcap" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    task "flask" {
      driver = "docker"
      config {
        image = "superboum/amd64_webcap:v7"
        port_map {
          web_port = 3000
        }
      }
      env {
        FLASK_APP = "/usr/local/bin/webcap"
      }

      resources {
        cpu = 1000
        memory = 2000
        network {
          port "web_port" {}
        }
      }

      service {
        tags = [
          "webcap",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:webcap.deuxfleurs.fr;PathPrefix:/"
        ]
        port = "web_port"
        address_mode = "host"
        name = "webcap"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

