job "mariadb" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    count = 1
    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_mariadb:v3"
        port_map {
          mariadb_port = 3306
        }
        command = "tail"
        args = [
          "-f", "/var/log/mysql/error.log",
        ]
        volumes = [
          "/mnt/glusterfs/mariadb/main/server:/var/lib/mysql",
        ]
      }

      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/mariadb/main/env.tpl?raw"
        destination = "secrets/env.tpl"
        mode = "file"
      }
      template {
        source = "secrets/env.tpl"
        destination = "secrets/env"
        env = true
      }


      resources {
        memory = 800
        network {
          port "mariadb_port" {
            static = "3306"
          }
        }
      }

      service {
        tags = ["mariadb"]
        port = "mariadb_port"
        address_mode = "host"
        name = "mariadb"
        check {
          type = "tcp"
          port = "mariadb_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

