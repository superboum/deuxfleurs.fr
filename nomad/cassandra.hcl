job "cassandra" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "scylladb" {
    count = 3

    constraint {
      operator  = "distinct_hosts"
      value     = "true"
    }

    // volume + command line
    task "scylladb_worker" {
      driver = "docker"
      config {
        image = "scylladb/scylla:3.1.2"
        readonly_rootfs = false
        port_map {
          scylla_port = 9042
          scylla_gossip_port = 7000
          scylla_jmx_port = 7199
        }
        args = [
          "--seeds=192.168.1.2,192.168.1.3,192.168.1.4",
          "--listen-address=0.0.0.0",
          "--broadcast-address=${NOMAD_IP_scylla_port}",
          "--broadcast-rpc-address=${NOMAD_IP_scylla_port}",
          "--developer-mode", "1",
          "--overprovisioned", "1",
          "--experimental", "0",
          "--memory", "200M"
        ]
	volumes = [
          "/var/lib/scylla:/var/lib/scylla"
        ]
      }

      resources {
        memory = 500
        network {
          port "scylla_port" {
            static = "9042"
          }
          port "scylla_gossip_port" {
            static = "7000"
          }
          port "scylla_jmx_port" {
            static = "7199"
          }
        }
      }

      service {
        tags = ["scylla"]
        port = "scylla_port"
        address_mode = "host"
        name = "scylla"
        check {
          type = "tcp"
          port = "scylla_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

