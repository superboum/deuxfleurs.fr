```bash
ssh root@<one node of the cluster>
docker run -t -i superboum/amd64_postgres:v1
psql -h psql-proxy.service.2.cluster.deuxfleurs.fr -p 25432 -U postgres -W postgres
```

```sql
CREATE USER seafile;
CREATE DATABASE seafile ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER seafile;
-- GRANT ALL PRIVILEGES ON DATABASE seafile TO seafile;
```

```
consul kv import @ldapkv_seafile.json
```
