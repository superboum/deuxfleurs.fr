This project is considered as "dangerous" as it is tagged as "Project not under active development".
Consequently, just in case, I am backuping the .jar and the sources in this git repo.
Better safe than sorry or pretty.

```
sudo docker build -t superboum/amd64_pithos:v1 .
sudo docker push superboum/amd64_pithos:v1
sudo docker run --rm -it -p 8080:8080 -v pithos.yaml:/etc/pithos/pithos.yaml superboum/amd64_pithos:v1
```
