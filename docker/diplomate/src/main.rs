use igd::aio::search_gateway;

#[tokio::main]
async fn main() {
    let gateway = match search_gateway(Default::default()).await {
        Ok(g) => g,
        Err(err) => return println!("Faild to find IGD: {}", err),
    };

    let pub_ip = match gateway.get_external_ip().await {
        Ok(ip) => ip,
        Err(err) => return println!("Failed to get external IP: {}", err),
    };
    println!("Our public IP is {}", pub_ip);
}
